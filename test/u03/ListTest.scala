package u03

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u02.Modules.Person

class ListTest {
  import Lists.List._
  import u02.Modules.Person._

  private final val lst = Cons (10 , Cons (20 , Cons (30 , Nil ())))
  @Test def dropTest(){

    assertEquals(Cons(20 , Cons(30 , Nil())), drop(lst ,1))   // Cons (20 , Cons (30 , Nil ()))
    assertEquals(Cons(30 , Nil()), drop(lst ,2))               // Cons (30 , Nil ())
    assertEquals(Nil(), drop(lst ,5))                           // Nil ()
  }

  @Test def flatMapTest(): Unit ={

    assertEquals(Cons (11 , Cons (21 , Cons (31 , Nil ()))),
      flatMap(lst)(v => Cons(v+1, Nil())))
    assertEquals(Cons (11 , Cons (12 , Cons (21 , Cons (22 , Cons (31 , Cons (32 , Nil ())))))),
      flatMap(lst)(v => Cons(v+1, Cons(v+2, Nil()))))
  }

  @Test def map2Test(){
    assertEquals(map(lst)(_+1), map2(lst)(_+1))
  }

  @Test def filter2Test(){
    assertEquals(filter(lst)(_>=20), filter2(lst)(_ >=20))
  }

  @Test def maxTest(){
    assertEquals(Some(25), max( Cons (10 , Cons (25 , Cons (20 , Nil ())))))
  }

  @Test def pListTest(){
    val personList = Cons[Person](Student("mario",2015), Cons(Student("luca",2014), Cons(Teacher("mirko","pps"), Nil())))
    assertEquals(Cons(Teacher("","pps"),Nil()), pList(personList))
  }


}

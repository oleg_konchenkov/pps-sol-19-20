package u03

import u02.Modules.Person
import u02.Modules.Person.Teacher

object Lists {

  // A generic linkedlist
  sealed trait List[E]

  // a companion object (i.e., module) for List
  object List {
    case class Cons[E](head: E, tail: List[E]) extends List[E]
    case class Nil[E]() extends List[E]

    def sum(l: List[Int]): Int = l match {
      case Cons(h, t) => h + sum(t)
      case _ => 0
    }

    def append[A](l1: List[A], l2: List[A]): List[A] = (l1, l2) match {
      case (Cons(h, t), l2) => Cons(h, append(t, l2))
      case _ => l2
    }

    def map[A,B](l: List[A])(mapper: A=>B): List[B] = l match {
      case Cons(h, t) => Cons(mapper(h), map(t)(mapper))
      case Nil() => Nil()
    }

    def filter[A](l1: List[A])(pred: A=>Boolean): List[A] = l1 match {
      case Cons(h,t) if (pred(h)) => Cons(h, filter(t)(pred))
      case Cons(_,t) => filter(t)(pred)
      case Nil() => Nil()
    }

    def drop[A](l: List[A], n: Int):List[A] = l match{
      case l if n == 0 => l
      case Cons(h,t) => drop(t, n-1)
      case Nil() => Nil()
    }

    def flatMap[A,B](l: List[A])(f: A => List[B]): List[B] = l match{
      case Cons(h,t)  => append(f(h), flatMap(t)(f))
      case Nil() => Nil()
    }

    def map2[A,B](l: List[A])(mapper: A=>B): List[B] = l match {
      case Nil() => Nil()
      case Cons(head, tail) => append(Cons(mapper(head), Nil()),flatMap(tail)(v=>Cons(mapper(v),Nil())))
    }

    def filter2[A](l: List[A])(pred: A=>Boolean): List[A] = l match {
      case Cons(h,t) if pred(h) => append(Cons(h,Nil()),flatMap(t)(v => Cons(v, Nil())))
      case Cons(_,t) => filter2(t)(pred)
      case Nil() => Nil()
    }

    def max(list: List[Int]): Option[Int] = {
      @scala.annotation.tailrec
      def maxRec(currentMax: Int, l: List[Int]): Option[Int] = l match{
        case Nil() => Some(currentMax)
        case Cons(head, tail) => maxRec(head.max(currentMax), tail)
      }
      maxRec(0, list)
    }

    import Person._
    def pList(l: List[Person]): List[Person] = {
      map(filter(l)(p => p.isInstanceOf[Teacher]))(p => Teacher("", course(p)))
    }


  }
}

object ListsMain extends App {
  import Lists._
  import Person._
  val l = List.Cons(10, List.Cons(20, List.Cons(30, List.Nil())))


}
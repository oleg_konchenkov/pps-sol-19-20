package lab2

object es4 extends App {
  def p3(x: Int)(y: Int)(z:Int): Boolean = x <= y && y <= z

  def p4(x: Int, y: Int, z: Int): Boolean = x <= y && y <= z

  val nonCurriedFunType: Int => Boolean = x => p4(x, x, x)
  println(nonCurriedFunType(2))

  def curriedMult(x: Double)(y: Double): Double = x*y

  val twice: Double => Double = curriedMult(2)
  val curriedFunType: Int=>Boolean = p3(2)(3)
  println(curriedFunType(4))
}

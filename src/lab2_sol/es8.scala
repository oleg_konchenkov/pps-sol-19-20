package lab2

object es8 extends App {

  sealed trait Option[A] // An Optional data type
  object Option {
    case class None[A]() extends Option[A]
    case class Some[A](a: A) extends Option[A]

    def isEmpty[A](opt: Option[A]): Boolean = opt match {
      case None() => true
      case _ => false
    }

    def getOrElse[A, B >: A](opt: Option[A], orElse: B): B = opt match {
      case Some(a) => a
      case _ => orElse
    }

    def flatMap[A,B](opt: Option[A])(f:A => Option[B]): Option[B] = opt match {
      case Some(a) => f(a)
      case _ => None()
    }

    def filter[A](opt: Option[A])(f: A => Boolean): Option[A] = opt match{
      case Some(a) if f(a) => Some(a)
      case _ => None()
    }

    def map[A](opt: Option[A])(f: A => Boolean): Option[Boolean] = opt match{
      case Some(a) if f(a) => Some(true)
      case _ => None()
    }

    def map2[A,B >: A](opt1: Option[A])(opt2: Option[B]): Option[B]= if (isEmpty(opt1) || isEmpty(opt2)) {
      None()
    }else{
      Some(opt1)
    }
  }

  //filter test
  val s4 :Option[Int] = Some(5)
  println(filter(s4)(_ > 2))
  println(filter(s4)(_ > 8))

  //map test
  println(map(s4)(_ > 2)) // Some(true)
  println(map(None[Int])(_ > 2)) // None

}

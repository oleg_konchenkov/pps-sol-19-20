package lab2

object es7 extends App {
  sealed trait Shape
  object Shape {

    case class Rectangle(lenght:Int, height: Int) extends Shape

    case class Circle(radius: Int) extends Shape

    case class Square(side:Int) extends Shape

  }
}

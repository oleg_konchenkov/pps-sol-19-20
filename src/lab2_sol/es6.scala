package lab2

object es6 extends App{
  def fib (n: Int): Int = n match{
    case 0 => 0
    case 1 => 1
    case a if a > 0 => fib(n-1) + fib(n-2)
  }

  println(fib(7))
}

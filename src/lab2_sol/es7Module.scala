package lab2



object es7Module {
  def perimeter(shape: Shape): Double = shape match{
    case Rectangle(lenght, height) => 2 * (lenght + height)
    case Shape.Square(side) => 4 * side
    case Shape.Circle(radius) => 2 * radius * math.Pi
  }
  def area(shape: Shape): Double = shape match{
    case Rectangle(lenght, height) => lenght * height
    case Shape.Square(side) => side * side
    case Shape.Circle(radius) => radius * radius * math.Pi
  }
}

package lab2

object es5 extends App{
  def compose(x: Int => Int, y: Int=> Int): Int => Int = a => x(y(a))
  println(compose(_-1,_*2)(5))

  def compose1[A](x: A => A, y: A=> A): A => A = a => x(y(a))
  println(compose1[Int](_-1,_*2)(5))
}

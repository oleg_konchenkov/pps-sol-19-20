package lab2

object Es3 extends App{
  //es 3a:
  def parity(d: Int): String = d match{
    case n if n % 2 == 0  => "even"
    case _ => "odd"
  }
  println(parity(10))
  println(parity(9))

  val parity2: Int => String = x => if (x % 2 == 0) {
    "even"
  }else{
    "odd"
  }
  println(parity2(10))
  println(parity2(9))

  //es 3b:
  val empty: String => Boolean = _=="" // predicate on strings

  val neg: (String => Boolean)  => (String => Boolean) = f  => !f(_)
  val notEmpty = neg(empty) // which type of notEmpty?
  println(notEmpty("foo"))// true
  println(notEmpty("")) // false
  println(notEmpty("foo") && !notEmpty("")) // true.. a comprehensive test

  def negate(pred: String => Boolean): String => Boolean= p => !pred(p)
  val notEmpty2 = negate(empty) // which type of notEmpty?
  println(notEmpty2("foo"))// true
  println(notEmpty2("")) // false
  println(notEmpty2("foo") && !notEmpty("")) // true.. a comprehensive test
  //es 3c:
  def genNegate[A,B](pred: A => B): A => B = pred match{
    case a if a == Boolean => a => pred(a)
    case _ => pred(_)
  }
  //Es 3c only A generic
  def neg[A](pred: A=>Boolean): (A=>Boolean) =  !pred(_)
  val notEmpty3 = genNegate[String,Boolean](empty)
  println(notEmpty3("foo"))// true
  println(notEmpty3("")) // false
  println(notEmpty3("foo") && !notEmpty("")) // true.. a comprehensive test

}
